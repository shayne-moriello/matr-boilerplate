Welcome to Curriculum-UI
A ReactJS web application.

Follow these instructions for developing, testing, and releasing.

Before anything, install your dependancies (node_modules)
1. npm install

Start dev server w/ hotloading:
1.  npm run start

Make sure code is tidy by running:
1. npm run lint

Run tests:
1.  npm run test

Build and bundle for production w/ optimizations:
1.  npm run build

To build and run local Docker image:
1. docker build -t matr-app .
2. docker run -p 8080:8080 -d matr-app