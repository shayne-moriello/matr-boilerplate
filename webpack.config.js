require('babel-polyfill');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');

module.exports = (env, argv) => ({
  entry: ['babel-polyfill', './src/index.jsx'], // build the dependancy graph starting from here
  output: {
    path: path.resolve(__dirname, 'dist'), // put the finished product here
    filename: 'bundle.[hash].js', // and name it this
    publicPath: '/', // so 404s fallback to index and allow client routing (also must start webpack dev server with --history-api-fallback)
  },
  devtool: argv.mode === 'production' ? 'source-map' : 'eval-source-map',
  resolve: {
    modules: ['node_modules', 'src'], // look in these places when I tell you to import something
    extensions: ['.js', '.jsx'], // don't make me write these extensions when importing
    mainFiles: ['index'], // if I don't specify a file when importing, assume this one
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/, // look for these file extensions when building dependancy graph
        exclude: /node_modules/, // except in these places
        use: {
          loader: 'babel-loader', // and use this loader to understand them
        },
      },
      {
        test: /\.(c|sc|sa)ss$/,
        use: [
          { loader: 'style-loader' },
          { loader: MiniCssExtractPlugin.loader },
          { loader: 'css-loader' },
          { loader: 'postcss-loader' },
          {
            loader: 'sass-loader',
            options: {
              includePaths: ['./node_modules'],
            },
          },
        ], // the order matters here! start from the last one and work backwards pipeing the output
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'style.[contenthash].css', // bundle all stylesheets into one file and name it this
    }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: './src/index.html', // start with this as the base
      filename: 'index.html', // and output this filling in the blanks
    }),
    new WebpackMd5Hash(),
    new CircularDependencyPlugin({
      // exclude detection of files based on a RegExp
      exclude: /a\.js|node_modules/,
      // add errors to webpack instead of warnings
      failOnError: true,
      // allow import cycles that include an asyncronous import,
      // e.g. via import(/* webpackMode: "weak" */ './file.js')
      allowAsyncCycles: false,
      // set the current working directory for displaying module paths
      cwd: process.cwd(),
    }),
  ],
});
