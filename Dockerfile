FROM node:8.11.1 as intermediate

RUN mkdir /builddir
WORKDIR /builddir

COPY package.json /builddir/package.json
COPY package-lock.json /builddir/package-lock.json
RUN npm install

COPY ./src /builddir/src
COPY ./webpack.config.js /builddir/webpack.config.js
COPY ./.babelrc /builddir/.babelrc
COPY ./postcss.config.js /builddir/postcss.config.js

RUN npm run build

FROM nginx:1.15.0-alpine
# copy the repository form the previous image
COPY --from=intermediate /builddir/dist /public/
RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/conf.d/matr-app-nginx.conf

WORKDIR /public

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
