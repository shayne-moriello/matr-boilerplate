import { timer } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

const genericRetryStrategy = ({
  maxRetryAttempts = 3,
  scalingDuration = 500,
  excludedStatusCodes = [],
} = {}) => attempts => attempts.pipe(mergeMap((error, i) => {
  const retryAttempt = i + 1;
  if (retryAttempt > maxRetryAttempts || excludedStatusCodes.find(e => e === error.status)) {
    throw error;
  }
  return timer(retryAttempt * scalingDuration);
}));

export default genericRetryStrategy;
