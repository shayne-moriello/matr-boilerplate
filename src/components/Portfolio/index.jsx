import React from 'react';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  Icon,
  IconButton,
  Input,
  InputAdornment,
  LinearProgress,
  List,
  ListItem,
  Typography
} from '@material-ui/core';

const styles = {
  card: {
    minWidth: 275,
    height: '100%'
  },
  cardContent: {
    height: 'auto'
  },
  cardRow: {
    height: 275
  },
  title: {
    marginBottom: 16,
    fontSize: 14
  }
};

const Portfolio = ({ stocks, removeFromPortfolio }) => {
  return (
    <Grid container spacing={24}>
      {stocks.map((s, i) => (
        <Grid item xs={6} sm={3} key={i} style={styles.cardRow}>
          <Card style={styles.card}>
            <CardContent>
              <Typography style={styles.title} color="textSecondary">
                {s.date}
              </Typography>
              <Typography variant="headline" gutterBottom>
                {s.name}
              </Typography>
              <Typography variant="caption" gutterBottom>
                {s.symbol}
              </Typography>
              <IconButton onClick={() => removeFromPortfolio(s)}>
                <Icon>delete</Icon>
              </IconButton>
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default Portfolio;
