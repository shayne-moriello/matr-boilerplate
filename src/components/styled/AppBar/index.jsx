import React from 'react';
import { default as MuiAppBar } from '@material-ui/core/AppBar';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import theme from '../../../themes';
import styles from './style';

const StyledAppBar = withStyles(styles)(({ classes, ...restProps }) => (
  <MuiAppBar classes={{ root: classes.root }} {...restProps} />
));

function AppBar(props) {
  const {
    position, color, classes, ...rest
  } = props;
  return (
    <MuiThemeProvider theme={theme}>
      <StyledAppBar classes={classes} position={position || 'static'} color={color || 'inherit'} {...rest}>{props.children}</StyledAppBar>
    </MuiThemeProvider>
  );
}

export default AppBar;
