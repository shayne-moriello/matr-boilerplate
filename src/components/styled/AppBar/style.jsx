const style = theme => ({
  root: {
    backgroundColor: `${theme.palette.primary.main}`,
  },
});

export default style;
