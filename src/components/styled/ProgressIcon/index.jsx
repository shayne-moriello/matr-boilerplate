import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import styles from './style';

const LensIcon = ({ fill, disabled }) => (
  <svg width={fill ? 28 : 27} height={fill ? 28 : 27}>
    {disabled &&
      <path
        fill="#A5AAB3"
        fillOpacity="1"
        stroke="#A5AAB3"
        strokeWidth="2"
        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2z"
      />
    }
    {!disabled &&
      <path
        fill={fill ? '#CE3D56' : '#FFF'}
        fillOpacity={fill ? '1' : '1'}
        stroke={fill ? '#FFF' : '#CE3D56'}
        strokeWidth={fill ? '0' : '2'}
        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2z"
      />
    }
  </svg>
);

export const Complete = ({ label = '', classes }) => (
  <div className={classes.icon}>
    <LensIcon fill />
    <div className={classes.labelWrapper}>
      <Typography className={classes.label} variant="body1" >{label}</Typography>
    </div>
  </div>
);

export const ProgressIconComplete = withStyles(styles)(Complete);

const Incomplete = ({ label = '', classes }) => (
  <div className={classes.icon}>
    <LensIcon />
    <div className={classes.labelWrapper}>
      <Typography className={classes.label} variant="body1">{label}</Typography>
    </div>
  </div>
);

export const ProgressIconIncomplete = withStyles(styles)(Incomplete);

const Disabled = ({ label = '', classes }) => (
  <div className={classes.icon}>
    <LensIcon disabled />
    <div className={classes.labelWrapper}>
      <Typography className={classes.label} variant="body1">{label}</Typography>
    </div>
  </div>
);

export const ProgressIconDisabled = withStyles(styles)(Disabled);

