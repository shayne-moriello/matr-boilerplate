export const style = theme => ({
  icon: {
    position: 'relative',
  },
  label: {
    color: `${theme.palette.common.white}`,
  },
  labelWrapper: {
    position: 'absolute',
    top: 2,
    left: -1,
    width: '100%',
    height: '100%',
    textAlign: 'center',
    lineHeight: '6px',
  },
});

export default style;
