export default function style(theme) {
  return ({
    root: {
      height: '38px',
      width: '186px',
      border: `2px solid ${theme.palette.primary.main}`,
      borderRadius: '20px',
      color: `${theme.palette.primary.main}`,
      '&:hover': { backgroundColor: `${theme.palette.common.white}` },
    },
  });
}

export function invertedStyle(theme) {
  return ({
    root: {
      height: '38px',
      width: '186px',
      border: `2px solid ${theme.palette.common.white}`,
      backgroundColor: `${theme.palette.primary.main}`,
      borderRadius: '20px',
      color: `${theme.palette.common.white}`,
      '&:hover': { backgroundColor: `${theme.palette.primary.main}` },
    },
  });
}
