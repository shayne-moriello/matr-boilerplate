import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import Button from './index';
import * as style from './style';
import theme from '../../../theme';

Enzyme.configure({ adapter: new Adapter() });

describe('styled/Button', () => {
  test('button text as prop', () => {
    const text = 'My Button';
    const wrapper = mount(<Button>{text}</Button>);
    const button = wrapper.find(Button);
    expect(button.text()).toBe(text);
  }),
  test('button onClick handler registers', () => {
    const spy = sinon.spy();
    const handler = () => spy();
    const wrapper = mount(<Button onClick={handler} />);
    wrapper.find(Button).simulate('click', { preventDefault() {} });
    expect(spy.called);
  }),
  test('button inverted', () => {
    const wrapper = mount(<Button inverted />);
    const button = wrapper.find(Button);
    expect(button.props()['inverted']).toBe(true);
  });
});

