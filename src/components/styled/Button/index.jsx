import React from 'react';
import PropTypes from 'prop-types';
import { default as MuiButton } from '@material-ui/core/Button';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import theme from '../../../themes';
import style, { invertedStyle } from './style';

const StyledButton = withStyles(style)(({ classes, ...restProps }) => (
  <MuiButton classes={classes} {...restProps} />
));
const InvertedStyledButton = withStyles(invertedStyle)(({ classes, ...restProps }) => (
  <MuiButton classes={classes} {...restProps} />
));

const Button = (props) => {
  const { inverted, classes, ...rest } = props;
  return (
    <MuiThemeProvider theme={theme}>
      {inverted ? (
        <InvertedStyledButton classes={classes} {...rest}>
          {props.children}
        </InvertedStyledButton>
      )
      : (
        <StyledButton classes={classes} {...rest}>
          {props.children}
        </StyledButton>
      )}
    </MuiThemeProvider>
  );
};

Button.propTypes = {
  inverted: PropTypes.bool,
  children: PropTypes.node,
  classes: PropTypes.object,
};

Button.defaultProps = {
  inverted: false,
  children: '',
  classes: {},
};

export default Button;
