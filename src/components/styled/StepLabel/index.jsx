import React from 'react';
import PropType from 'prop-types';

import { default as MuiStepLabel } from '@material-ui/core/StepLabel';
import { StepIconComplete, StepIconIncomplete } from '../StepIcon';

const StepLabel = ({ complete, classes }) =>
  (<MuiStepLabel icon={complete ? <StepIconComplete /> : <StepIconIncomplete />} classes={classes} />);

StepLabel.propTypes = {
  complete: PropType.bool,
};

StepLabel.defaultProps = {
  complete: false,
};

export default StepLabel;
