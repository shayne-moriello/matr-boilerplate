import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { paths } from '../../constants';
import { withAuth } from '../../containers/withAuth';

const PrivateRoute = ({
  component: Component, isAuthenticated, ...rest
}) => (
  <Route
    {...rest}
    render={props => (
      isAuthenticated
        ? <Component {...props} />
        : <Redirect to={paths.TUTOR_APPLICATION_HOME} />
    )}
  />
);

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

export default withAuth(PrivateRoute);
