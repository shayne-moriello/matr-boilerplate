import React from 'react';
import { Route, Redirect, Switch, withRouter } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import { paths } from '../../constants';
import HomeScreen from '../../screens/HomeScreen';
import NotFound404 from '../../screens/NotFound404';

export const redirectTo = to => () => <Redirect to={to} />;

const Routes = () => (
  <Switch>
    <Route exact path={paths.INDEX} component={HomeScreen} />
    <Route component={NotFound404} />
  </Switch>
);

export default withRouter(Routes);
