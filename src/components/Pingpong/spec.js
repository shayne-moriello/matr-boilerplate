import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Pingpong from './index';
import { PING, PONG } from '../../constants';

Enzyme.configure({ adapter: new Adapter() });

describe('<Pingpong />', () => {
  test('button text is ping when pong', () => {
    const wrapper = mount(<Pingpong isPing />);
    const button = wrapper.find('button');
    expect(button.text()).toBe(`Show ${PONG}`);
  }),
  test('button text is pong when ping', () => {
    const wrapper = mount(<Pingpong isPing={false} />);
    const button = wrapper.find('button');
    expect(button.text()).toBe(`Show ${PING}`);
  });
});
