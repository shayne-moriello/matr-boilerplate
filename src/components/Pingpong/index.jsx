import React from 'react';
import { PING, PONG } from '../../constants';

const Pingpong = ({ name, value, isPing, toggle }) => {
  return (
    <div>
      <h1>Welcome to Mindgap Boilerplate, {name}</h1>
      <button onClick={toggle}>Show {isPing ? PONG : PING}</button>
      <div>{value}</div>
    </div>
  );
};

export default Pingpong;
