import React, { Component } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TutorApplication from '../../components/TutorApplication';
import { withAuth } from '../../containers/withAuth';
import { paths } from '../../constants';
import * as slices from '../../store/selectors';
import VerifyEmail from '../../screens/VerifyEmail';

class Auth0Callback extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { handleAuth } = this.props;
    handleAuth();
  }

  render() {
    const { isAuthenticated } = this.props;

    const params = {};
    window.location.search.substr(1).split('&').forEach((item) => {
      const kv = item.split('=');
      params[kv[0]] = decodeURIComponent(kv[1]);
    });

    let redirectTo = null;
    if (params.returnPath && params.returnPath.charAt(0) === '/') {
      redirectTo = params.returnPath;
    }

    const unverifiedEmail = !!this.props.location.hash.match('unverified_email');

    let content;
    if (unverifiedEmail) {
      content = <VerifyEmail />;
    } else if (isAuthenticated && redirectTo) {
      content = <Redirect to={redirectTo} />;
    } else {
      content = <TutorApplication />;
    }

    return (content);
  }
}

Auth0Callback.propTypes = {
  handleAuth: PropTypes.func.isRequired,
};

const View = withAuth(Auth0Callback);

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(View);
