/* eslint-disable import/prefer-default-export */
/* eslint-disable no-undef */
import auth0 from 'auth0-js';

const config = {
  domain: 'matr.eu.auth0.com',
  clientID: 'mGmVFpW82qLc1zXJ5IBfyTCxctHf6ec6',
  // redirectUri: 'https://learn.matr.org/callback?returnPath=',
  // redirectUri: 'http://local.thirdspacelearning.com:8080/callback?returnPath=',
  redirectUri: 'http://localhost:8080/callback',
  audience: 'http://matr.org/profile-api/',
  responseType: 'token id_token',
  scope: 'openid profile email',
};

export const getAccessToken = () => localStorage.getItem('accessToken');
export const getIdToken = () => localStorage.getItem('idToken');
export const getTokenExpirationTimestamp = () => localStorage.getItem('expiresAt');
export const getUser = () => JSON.parse(localStorage.getItem('user'));

class Auth {
  auth0 = new auth0.WebAuth(config);
  refreshTokenTimeoutId = null;

  login = (returnPath = '') => {
    this.auth0.authorize({
      redirectUri: config.redirectUri + encodeURIComponent(returnPath),
    });
  }

  logout = () => {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('idToken');
    localStorage.removeItem('expiresAt');
    localStorage.removeItem('user');
    this.auth0.logout({
      returnTo: 'https://talent.matr.org',
    });
  }

  isAuthenticated = () => {
    const expiresAt = JSON.parse(getTokenExpirationTimestamp());
    return (expiresAt !== null) && (new Date().getTime() < expiresAt);
  }

  fetchUser = () => {
    this.auth0.client.userInfo(getAccessToken(), (err, profile) => {
      if (profile) {
        
        if (profile[`https://matr.org/profile-api/user_metadata`]) {
          profile = {
            ...profile,
            ...profile[`https://matr.org/profile-api/user_metadata`]
          }
        }

        localStorage.setItem('user', JSON.stringify(profile));
      } else {
        console.log(err);
      }
    });
  }

  handleAuthentication = (success, fail) => new Promise((resolve, reject) => {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        this.fetchUser();
        resolve(success());
      }

      if (err) {
        reject(fail(err));
      }
    });
  })

  scheduleRenewal = () => {
    const delay = getTokenExpirationTimestamp() - Date.now();
    if (delay > 0) {
      clearTimeout(this.refreshTokenTimeoutId);
      this.refreshTokenTimeoutId = setTimeout(this.renewToken, delay);
    }
  }

  renewToken = () => {
    this.auth0.checkSession({}, (err, result) => {
      if (err) {
        // eslint-disable-next-line
        console.error(err);
      } else {
        this.setSession(result);
        this.fetchUser();
      }
    });
  }

  setSession = (authResult) => {
    localStorage.setItem('accessToken', authResult.accessToken);
    localStorage.setItem('idToken', authResult.idToken);
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('expiresAt', expiresAt);
    // this.scheduleRenewal();
  }
}

const auth = new Auth();
Object.freeze(auth);

export default auth; // export singleton
