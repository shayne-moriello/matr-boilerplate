/* eslint-disable import/prefer-default-export */
import { getAccessToken } from '../auth';

const API_ORIGIN = 'https://api.matr.org/api/v1';
const UPLOAD_API_ORIGIN = 'https://learn.matr.org/generic-upload-api/api/v1';
const SAML_PAYLOAD_API_ORIGIN = 'https://tlms-api.matr.org/saml_payload';

const defaultOptions = {
  withAuth: true,
};

const defaultPostOptions = {
  withAuth: false,
};

const getCommonHeaders = (options) => {
  const headers = {};
  if (options.withAuth) {
    headers.Authorization = `Bearer ${getAccessToken()}`;
  }

  return headers;
};

export const objectToQueryParams = (object = {}) => Object.keys(object)
  .reduce((result, key) => {
    const value = object[key];
    if (value !== undefined && value !== null) {
      result.push(`${key}=${value}`);
    }
    return result;
  }, [])
  .join('&');

const appendParamsToUrl = (url, params) => {
  const query = objectToQueryParams(params);
  return params
    ? `${url}?${query}`
    : url;
};

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }

  return response;
}

export const get = (url, params, additionalConfig = {}, options = defaultOptions) => fetch(
  appendParamsToUrl(url, params),
  {
    credentials: 'omit',
    mode: 'cors',
    ...additionalConfig,
    headers: {
      Accept: 'application/json',
      ...getCommonHeaders(options),
      ...additionalConfig.headers,
    },
  },
).then(handleErrors);

export const toJSON = response => response.json();

export const post = (url, payload, additionalConfig = {}, options = defaultPostOptions) => fetch(url, {
  credentials: 'omit',
  mode: 'cors',
  method: 'POST',
  body: JSON.stringify(payload),
  ...additionalConfig,
  headers: {
    'content-type': 'application/json',
    ...getCommonHeaders(options),
    ...additionalConfig.headers,
  },
}).then(handleErrors);

export const postWithAuth = (url, payload, additionalConfig = {}, options = defaultOptions) => fetch(url, {
  credentials: 'include',
  mode: 'cors',
  method: 'POST',
  body: JSON.stringify(payload),
  ...additionalConfig,
  headers: {
    'content-type': 'application/json',
    ...getCommonHeaders(options),
    ...additionalConfig.headers,
  },
}).then(handleErrors);

export const postForm = (url, form, additionalConfig = {}, options = defaultOptions) => fetch(url, {
  credentials: 'include',
  mode: 'cors',
  method: 'POST',
  body: form,
  ...additionalConfig,
  headers: {
    ...getCommonHeaders(options),
    ...additionalConfig.headers,
  },
}).then(handleErrors);

const retrieveUploadParameters = (filename, namespace) => postWithAuth(`${UPLOAD_API_ORIGIN}/uploads`, {
  filename,
  namespace,
  app_name: 'tutor-application',
  profile: 'tutor',
}).then(toJSON);

export const uploadFile = async (file, filename = null, namespace = 'unknown') => {
  const fN = filename || file.name;
  const { upload_parameters: uploadParameters } = await retrieveUploadParameters(fN, namespace);
  const { url, fields } = uploadParameters;

  const formData = new FormData();
  formData.append('Content-Disposition', `attachment; filename=${fN}`);
  Object.keys(fields).forEach((key) => {
    formData.append(key, fields[key]);
  });
  formData.append('file', file);

  const config = { credentials: 'omit' };
  await postForm(url, formData, config, { withAuth: false });
  const fieldKeys = fields.key.split('/'); // const uploadId = last(fields.key.split('/'));
  const fileId = fieldKeys[fieldKeys.length - 1]; // use last

  return fields.key;
};


export const getSamlPayload = async (params) => {
  const config = { credentials: 'omit' };
  const options = { withAuth: true };
  const response = await post(SAML_PAYLOAD_API_ORIGIN, params, config, options);

  if (response.status === 200) {
    return {
      success: true,
      data: await response.json(),
    };
  }
  return { success: false };
};

export const saveForm = body => post(`${API_ORIGIN}/applicant/register`, body)
  .then(toJSON)
  .then(res => console.log(res))
  .catch(err => console.error(err));

export const loadUserData = id => get(`${API_ORIGIN}/applicants/search/${id}`)
  .then(toJSON)
  .catch(err => console.error(err));
