/*
** App entrypoint. Initializes Router, Store, and mounts app in specified element.
*/
import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import history from './history';
import store from './store';
import theme from './themes';
import './index.scss';

import App from './components/App';

ReactDOM.render(
  (
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <Router history={history}>
          <App />
        </Router>
      </Provider>
    </MuiThemeProvider>
  ), document.getElementById('app'),
);
