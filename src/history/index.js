import createHistory from 'history/createBrowserHistory';
import { BASENAME } from '../constants';

const history = createHistory({
  basename: BASENAME,
});

export default history;
