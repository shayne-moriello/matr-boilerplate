import 'rxjs';
import { syncHistoryWithStore } from 'react-router-redux';
import { applyMiddleware, createStore } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import history from '../history';
import * as api from '../api';
import rootReducer from './reducer';
import rootEpic from './epic';

const epicOptions = {
  dependencies: { api },
};
const epicMiddleware = createEpicMiddleware(rootEpic, epicOptions);
const thunkMiddleware = thunk.withExtraArgument(api);
const rootMiddleware = applyMiddleware(
  epicMiddleware,
  thunkMiddleware,
);

const store = createStore(
  rootReducer,
  composeWithDevTools(rootMiddleware),
);

syncHistoryWithStore(history, store);

export default store;

