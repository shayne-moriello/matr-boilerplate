import auth, { getAccessToken } from '../../auth';

export const STATE_KEY = 'auth';
export const selectSlice = state => state[STATE_KEY];

export const LOGIN = 'login';
const login = () => ({
  type: LOGIN,
});

export const LOGIN_SUCCESS = 'login-success';
const loginSuccess = () => ({
  type: LOGIN_SUCCESS,
});

export const LOGIN_FAILED = 'login-failed';
const loginFailed = error => ({
  type: LOGIN_FAILED,
  payload: error,
});

export const LOGOUT = 'logout';
const logout = () => ({
  type: LOGOUT,
});

export const loginUser = (returnPath) => (dispatch) => {
  dispatch(login());
  auth.login(returnPath);
};

export const handleAuth = () => dispatch => auth.handleAuthentication(loginSuccess, loginFailed).then((success) => {
  dispatch(success);
}).catch((err) => {
  dispatch(err);
});

export const logoutUser = () => (dispatch) => {
  auth.logout();
  dispatch(logout());
};

export const INITIAL_STATE = {
  isAuthenticated: auth.isAuthenticated(),
  loading: false,
  error: null,
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loading: true,
      };
    case LOGIN_FAILED:
      return {
        ...state,
        loading: false,
        isAuthenticated: false,
        error: action.payload,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        isAuthenticated: true,
      };
    case LOGOUT:
      return {
        ...state,
        loading: false,
        isAuthenticated: false,
      };

    default:
      return { ...state };
  }
}
