import axios from 'axios';
import { from, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  mergeMap,
  retryWhen,
  switchMap,
  take,
  takeUntil,
  toArray,
} from 'rxjs/operators';
import { ofType } from 'redux-observable';
import genericRetryStrategy from '../../utils/retryStrategy';

export const STATE_KEY = 'search';
export const selectSlice = state => state[STATE_KEY];

export const QUERY = 'query';
export const QUERY_RETURNED = 'query_returned';
export const CANCEL_QUERY = 'cancel_query';
export const NETWORK_ERROR = 'network_error';
export const RELAY = 'relay';

export const INITIAL_STATE = {
  results: [],
  isPreLoading: false,
  isLoading: false,
  error: null,
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case QUERY:
      return {
        ...state,
        results: [],
        isPreLoading: true,
        isLoading: false,
        error: null,
      };
    case CANCEL_QUERY:
      return {
        ...state,
        results: [],
        isPreLoading: false,
        isLoading: false,
        error: null,
      };
    case RELAY:
      return {
        ...state,
        isPreLoading: false,
        isLoading: true,
      };
    case QUERY_RETURNED:
      return {
        ...state,
        results: action.payload,
        isPreLoading: false,
        isLoading: false,
      };
    case NETWORK_ERROR:
      return {
        ...state,
        results: [],
        isPreLoading: false,
        isLoading: false,
        error: action.error,
      };

    default:
      return { ...state };
  }
}

const query = term => ({
  type: QUERY,
  payload: term,
});

const queryReturned = results => ({
  type: QUERY_RETURNED,
  payload: results,
});

const cancelQuery = () => ({
  type: CANCEL_QUERY,
});

const relayQuery = term => ({
  type: RELAY,
  payload: term,
});

const networkError = err => ({
  type: NETWORK_ERROR,
  error: err,
});

const bonifyObjects = (delayedQuotes, symbols) => symbols.map(s => ({
  ...s,
  ...(delayedQuotes.filter(dq => dq.symbol === s.symbol)[0] || {}),
}));

const handleError = error => of(networkError(error));

const handleResponse = (response, action) => {
  const term = action.payload.toUpperCase();
  const filteredData =
    response.data.filter(d => d.symbol.substr(0, term.length).toUpperCase() === term.toUpperCase()); // starts with term
  const exactMatch = filteredData.map(d => d.symbol).indexOf(term); // find any exact matches
  if (exactMatch > -1) {
    // if any
    filteredData.unshift(filteredData[exactMatch]); // insert exact match to top of list
    filteredData.splice(exactMatch + 1, 1); // index shifted by 1 due to insert above
  }
  return filteredData;
};

export const queryEpic = action$ => action$.pipe(
  ofType(QUERY),
  debounceTime(300),
  switchMap((action) => {
    const term = action.payload.trim();
    if (term === '') {
      return of(cancelQuery());
    }
    // hit normalizr cache
    // is the data stale? (define stale)
    return of(relayQuery(term.toUpperCase()));
  }),
  catchError(e => handleError(e, [])),
);

export const relayEpic = action$ => action$.pipe(
  ofType(RELAY),
  switchMap(relayAction =>
    from(axios.get('https://api.iextrading.com/1.0/ref-data/symbols')).pipe(
      switchMap((res) => {
        const filteredSymbols = handleResponse(res, relayAction);
        // store in normalizr cache
        return of(filteredSymbols);
      }),
      retryWhen(genericRetryStrategy()),
      takeUntil(action$.ofType(CANCEL_QUERY)),
      catchError(e => handleError(e, [])),
    )),
  // define error handling here:
  // what happens when 1/many calls fail?
  // skip and continue (return individual 'unable to get price' message)? or
  // return where we are at?
  switchMap(symbolObjects => from(symbolObjects).pipe(
    take(10),
    mergeMap(o => from(axios.get(`https://api.iextrading.com/1.0/stock/${o.symbol}/delayed-quote`))),
    toArray(),
    mergeMap((responseArray) => {
      const bonifiedSymbols = bonifyObjects(responseArray.map(r => r.data), symbolObjects);
      // store in normalizr cache
      return of(queryReturned(bonifiedSymbols));
    }),
  )),
);

export const search = q => (dispatch) => {
  const tryValueFromState = q || '';
  if (tryValueFromState !== '') dispatch(query(tryValueFromState));
  dispatch(cancelQuery());
};

export const typeahead = event => (dispatch) => {
  const tryValueFromInput = event.target.value ? event.target.value : '';
  if (tryValueFromInput !== '') dispatch(query(tryValueFromInput));
  dispatch(cancelQuery());
};

export const cancelSearch = () => (dispatch) => {
  dispatch(cancelQuery());
};
