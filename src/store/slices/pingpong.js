import { PING, PONG } from '../../constants';

export const STATE_KEY = 'pingpong';

export const TOGGLE = 'toggle';

export const INITIAL_STATE = {
  value: PING,
  isPing: true,
};

const toggle = payload => ({
  type: TOGGLE,
  payload,
});

export const selectSlice = state => state[STATE_KEY];

export const asyncToggle = () => (dispatch, getState, api) => api(() => {
  const state = getState();
  const pingpong = selectSlice(state);
  const newValue = pingpong.isPing ? PONG : PING;
  dispatch(toggle({ value: newValue, isPing: !pingpong.isPing }));
});

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TOGGLE:
      return {
        ...state,
        value: action.payload.value,
        isPing: action.payload.isPing,
      };

    default:
      return { ...state };
  }
}
