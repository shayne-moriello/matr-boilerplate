import { combineEpics } from 'redux-observable';

import { queryEpic, relayEpic } from './slices/search';

export default combineEpics(queryEpic, relayEpic);
