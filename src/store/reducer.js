import { combineReducers } from 'redux';
import reduceReducers from 'reduce-reducers';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import auth, {
  INITIAL_STATE as AUTH_INITIAL_STATE,
  STATE_KEY as AUTH_STATE_KEY,
} from './slices/auth';

function getInitialState() {
  return {
    ...AUTH_INITIAL_STATE,
  };
}

const reducer = combineReducers(
  {
    [AUTH_STATE_KEY]: auth,
    routing: routerReducer,
  },
  getInitialState(),
);

export default reducer;
