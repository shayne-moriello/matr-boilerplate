import { connect } from 'react-redux';
import * as actions from '../store/actions';
import * as slices from '../store/selectors';

import { default as View } from '../components/Pingpong';

const mapStateToProps = state => ({
  ...slices.selectPingpong(state),
  name: 'Matr',
});

const mapDispatchToProps = dispatch => ({
  toggle: () => dispatch(actions.asyncToggle()),
});

const Pingpong = connect(
  mapStateToProps,
  mapDispatchToProps,
)(View);

export default Pingpong;
