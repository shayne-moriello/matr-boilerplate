import { connect } from 'react-redux';
import * as actions from '../store/actions';
import * as slices from '../store/selectors';

import { default as View } from '../components/Portfolio';

const mapStateToProps = state => {
  return {
    ...slices.selectPortfolio(state)
  }
};

const mapDispatchToProps = dispatch => {
  return {
    removeFromPortfolio: (stock) => dispatch(actions.removeFromPortfolio(stock))
  }
};

const Portfolio = connect(
  mapStateToProps,
  mapDispatchToProps
)(View);

export default Portfolio;
