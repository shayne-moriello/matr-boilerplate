/* eslint-disable import/prefer-default-export */
import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import * as slices from '../../store/selectors';

const mapStateToProps = state => ({
  ...slices.selectAuth(state),
});

const mapDispatchToProps = dispatch => ({
  loginUser: (returnPath = '') => dispatch(actions.loginUser(returnPath)),
  logoutUser: () => dispatch(actions.logoutUser()),
  handleAuth: () => dispatch(actions.handleAuth()),
});

export const withAuth = Component => connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false }, // https://github.com/reduxjs/react-redux/blob/master/docs/troubleshooting.md#my-views-arent-updating-when-something-changes-outside-of-redux
)(Component);
