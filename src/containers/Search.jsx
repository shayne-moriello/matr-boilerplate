import { connect } from 'react-redux';
import * as actions from '../store/actions';
import * as slices from '../store/selectors';

import { default as SearchView } from '../components/Search';

const mapStateToProps = state => {
  return {
    ...slices.selectSearch(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    search: (q) => dispatch(actions.search(q)),
    typeahead: (e) => dispatch(actions.typeahead(e)),
    cancel: () => dispatch(actions.cancelSearch()),
    addToPortfolio: (stock) => dispatch(actions.addToPortfolio(stock))
  };
};

const Search = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchView)

export default Search;