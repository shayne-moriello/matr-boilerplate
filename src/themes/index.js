/* Find default theme parameters here: https://material-ui.com/customization/default-theme/ */
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#CE3D56',
      light: '#EF6A6E',
      dark: '#92182D',
    },
    secondary: {
      main: '#16253D',
    },
  },
  typography: {
    fontFamily: [
      'Muli',
    ].join(','),
  },
  display4: {
    fontFamily: [
      'Muli',
    ].join(','),
    fontSize: '34px',
    fontWeight: '300',
    lineHeight: '45px',
    color: '#030A15',
  },
  overrides: {
    MuiPaper: {
      root: {
        borderRadius: '0px', // 4px
      },
    },
    MuiMenuItem: {
      root: {
        '&$selected': {
          backgroundColor: '#47556A',
        },
      },
    },
    MuiStepper: {
      root: {
        paddingLeft: '0px',
        backgroundColor: '#F2F4F8',
      },
    },
    MuiStep: {
      root: {
      },
    },
    MuiStepContent: {
      root: {
        marginTop: '-40px',
        paddingLeft: '28px',
        border: '0',
        borderStyle: 'none',
      },
    },
    MuiCollapse: {
      container: {
        overflow: 'visible', // allows select options to be visible
      },
    },
    MuiStepConnector: {
      lineVertical: {
        border: '0',
        borderStyle: 'none !important',
      },
    },
    MuiDrawer: {
      paper: {
        overflowY: 'none',
      },
    },
  },
});

export default theme;
