import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import HomeIcon from '@material-ui/icons/Home';
import Button from '../components/styled/Button';
import { paths } from '../constants';

const style = {
  root: {
    flexGrow: 1,
  },
  message: {
    marginTop: '86px',
    textAlign: 'center',
  },
  welcomeText: {
    color: '#030A15',
    fontFamily: 'Muli',
    textAlign: 'center',
  },
  subtext: {
    color: '#16253D',
    fontFamily: 'Muli',
    fontSize: '18px',
    lineHeight: '22px',
    textAlign: 'center',
  },
  nextButton: {
    marginTop: '32px',
  },
  iconToLeft: {
    height: '16px',
    width: '16px',
    paddingBottom: '1px',
    paddingRight: '9px',
  },
  pageArea: {
    marginTop: '45px',
    marginLeft: '317px',
  },
  progress: {
    height: '172px',
  },
};


const NotFound404 = ({ history }) => (
  <div style={style.root}>
    <Grid container justify="center">

      <Grid style={style.message} item xs={6}>
        <Grid container item justify="center">
          <Grid item xs={12}>
            <Typography style={style.welcomeText} variant="title">Oops, nothing to see here.</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="headline">404 Not Found</Typography>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid container justify="center">
            <Grid item style={style.nextButton}>
              <Button onClick={() => history.push(paths.INDEX)}><HomeIcon style={style.iconToLeft} /> Go home</Button>
            </Grid>
          </Grid>
        </Grid>

      </Grid>
    </Grid>
  </div>
);

export default withRouter(NotFound404);
